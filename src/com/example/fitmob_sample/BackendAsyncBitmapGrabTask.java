package com.example.fitmob_sample;

import java.io.InputStream;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

public class BackendAsyncBitmapGrabTask extends AsyncTask<String, Void, Bitmap> {
	
	private AsyncBitmapTaskCompleteListener<Bitmap> callback;
	private Context mContext;
	
	public BackendAsyncBitmapGrabTask( Context context, AsyncBitmapTaskCompleteListener cb )
	{
		this.callback = cb;
		this.mContext = context;
	}
	
    @Override
    protected Bitmap doInBackground(String... urls) {
    
      InputStream content = null;
    	
      for (String url : urls) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        
        try {
          HttpResponse execute = client.execute(httpGet);
          content = execute.getEntity().getContent();

        } catch (Exception e) {
          e.printStackTrace();
        }
      }
      
      // we need to downsample the input images to preserve memory.
      final BitmapFactory.Options options = new BitmapFactory.Options();
      options.inSampleSize = 2;
      
      return BitmapFactory.decodeStream(content, null, options);
    }

    @Override
    protected void onPostExecute(Bitmap result) {
    	callback.onBitmapTaskComplete(result);
    }
    
    
    public interface AsyncBitmapTaskCompleteListener<T> {
    	   public void onBitmapTaskComplete(T result);
    }
    
}
