package com.example.fitmob_sample;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.text.format.Time;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class ImageAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<Drawable> mThumbIds;

    public ImageAdapter(Context c) {
        mContext = c;
        mThumbIds = new ArrayList<Drawable>();
    }

    public int getCount() {
        return mThumbIds.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }
    
    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        
        // Use parent width to determine how big our square imageview should be.
        int parentWidth = parent.getWidth();
           
        if (convertView == null) {  // if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(parentWidth/2, parentWidth/2));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setRotation(180);
        } else {
            imageView = (ImageView) convertView;
        }

        imageView.setImageDrawable(mThumbIds.get(position));
        return imageView;
    }
    
    
    // Add image to our private ArrayList and redraw
    // GridView
    public void addImage( Drawable img ) {
    	mThumbIds.add( img );
    	this.notifyDataSetChanged();
    }
    
    // save the image when sharing it
    public Uri getImageStream( int position ) {
    	Drawable img = mThumbIds.get(position);
       
    	BitmapDrawable bitDw = ((BitmapDrawable) img);
        Bitmap bitmap = bitDw.getBitmap();
 
        Matrix matrix = new Matrix();
        matrix.postRotate(180);
        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                
        Time now = new Time();
      
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File imageFile = new File(path, now.toMillis(false) + ".png");
        FileOutputStream fileOutPutStream = null;
		try {
			fileOutPutStream = new FileOutputStream(imageFile);
	        bitmap.compress(Bitmap.CompressFormat.PNG, 80, fileOutPutStream);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			fileOutPutStream.flush();
	        fileOutPutStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
     
        return Uri.parse("file://" + imageFile.getAbsolutePath());
    	
    }
     
}