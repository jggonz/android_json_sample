package com.example.fitmob_sample;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.os.AsyncTask;

public class BackendAsyncTask extends AsyncTask<String, Void, String> {
	
	private AsyncTaskCompleteListener<String> callback;
	private Context mContext;
	
	public BackendAsyncTask( Context context, AsyncTaskCompleteListener cb )
	{
		this.callback = cb;
		this.mContext = context;
	}
	
    @Override
    protected String doInBackground(String... urls) {
      String response = "";
      for (String url : urls) {
        DefaultHttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        try {
          HttpResponse execute = client.execute(httpGet);
          InputStream content = execute.getEntity().getContent();

          BufferedReader buffer = new BufferedReader(new InputStreamReader(content));
          String s = "";
          while ((s = buffer.readLine()) != null) {
            response += s;
          }

        } catch (Exception e) {
          e.printStackTrace();
        }
      }
      return response;
    }

    @Override
    protected void onPostExecute(String result) {
    	callback.onTaskComplete(result);
    }
    
    
    public interface AsyncTaskCompleteListener<T> {
    	   public void onTaskComplete(T result);
    }
    
}
