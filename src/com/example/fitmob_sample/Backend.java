package com.example.fitmob_sample;

import com.example.fitmob_sample.BackendAsyncBitmapGrabTask.AsyncBitmapTaskCompleteListener;
import com.example.fitmob_sample.BackendAsyncTask.AsyncTaskCompleteListener;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

public class Backend implements AsyncTaskCompleteListener<String>, AsyncBitmapTaskCompleteListener<Bitmap> {
	
	private Context mContext;
	private BackendTaskCompleteListener<String> callback;

	public Backend( Context context, BackendTaskCompleteListener cb ) {
		this.mContext = context;		
		this.callback = cb;
	}
	
	public void getJSON( ) {
		
		String url = "http://hoantonthat.com/imgur.json";
		
		BackendAsyncTask aT = new BackendAsyncTask( mContext, this );
		aT.execute( url );
		
	}
	
	public void getImage ( String url ) {
		BackendAsyncBitmapGrabTask aT = new BackendAsyncBitmapGrabTask( mContext, this );
		aT.execute(url);		
	}
	

	@Override
	public void onTaskComplete(String result) {
		callback.onBackendComplete(result);
	}
	
    public interface BackendTaskCompleteListener<T> {
 	   public void onBackendComplete(T result );
 	   public void onBackendBitmapComplete( Drawable result );
    }

	@Override
	public void onBitmapTaskComplete(Bitmap result) {
		Drawable bgD = new BitmapDrawable(mContext.getResources(), result);
		callback.onBackendBitmapComplete(bgD);
	}
	
}
