package com.example.fitmob_sample;


import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.Toast;

import com.example.fitmob_sample.Backend.BackendTaskCompleteListener;
import com.example.fitmob_sample.BackendAsyncTask.AsyncTaskCompleteListener;

public class MainActivity extends Activity {
	
	static Context mContext;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		mContext = this;
		
		Log.d("MainActivity", "Main Activity is Live");
		
		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment implements BackendTaskCompleteListener<String>, AsyncTaskCompleteListener<Bitmap> {

		ArrayList<String> urls;
		ArrayList<Drawable> images;
		ImageAdapter imgAdapter;
		Backend backend;
		
		public PlaceholderFragment() {
			
			// initiate the JSON request
			// callback is this.onBackendComplete()
			backend = new Backend( this.getActivity(), this );
			backend.getJSON();
			
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fitmob, container,
					false);
			
			imgAdapter = new ImageAdapter( this.getActivity() );
			
			GridView gridview = (GridView) rootView.getRootView().findViewById(R.id.gridview);
		    gridview.setAdapter( imgAdapter );

		    gridview.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					
					Toast.makeText(mContext, "Sharing image at position " + position, Toast.LENGTH_SHORT).show();

				    Intent shareIntent = new Intent();
				    shareIntent.setAction(Intent.ACTION_SEND);
				    shareIntent.putExtra(Intent.EXTRA_STREAM, imgAdapter.getImageStream(position));
				    shareIntent.setType("image/png");
				    startActivity(Intent.createChooser(shareIntent, "Share Image"));
				}
		    });
					
			return rootView;
		}
		
		@Override
		public void onBackendComplete(String result ) {
			
			urls = new ArrayList<String>();
			images = new ArrayList<Drawable>();
						
			try { 
				JSONObject obj = new JSONObject( result );
				
				JSONArray items = obj.getJSONArray("data");
				
				for( int x = 0; x < items.length(); x++ ) {
					
					JSONObject anItem = items.getJSONObject(x);
					
					String aLink = anItem.getString( "link" );
					
					urls.add( aLink );
					
					//Log.d( "Link", aLink );
					
					Backend imageGrabber = new Backend( this.getActivity(), this );
					imageGrabber.getImage(aLink);
			    	    	
				}
				
			} catch (Throwable t) {
			    Log.e("My App", "Could not parse malformed JSON: \"" + result + "\"");
			}
						
		}

		@Override
		public void onBackendBitmapComplete(Drawable result ) {
						
			BitmapDrawable bd = (BitmapDrawable) result;
			
			// Some links in the JSON feed are not really images.
			// Converting them to a BitmapDrawable and checking if
			// the conversion was successful helps us identify and 
			// ignore bogus links.
			if ( ! ( bd.getBitmap() == null ) ) {
				imgAdapter.addImage(result);
			}
		}

		@Override
		public void onTaskComplete(Bitmap result) {
			// TODO Auto-generated method stub
			
		}
	}
	
}



